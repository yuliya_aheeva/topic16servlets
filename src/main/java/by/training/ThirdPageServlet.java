package by.training;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class ThirdPageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Object goods = session.getAttribute("basket");
        Object name = session.getAttribute("user");
        Object total = session.getAttribute("total");
        resp.getWriter().println("<html>\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>order</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <div align=\"center\" margin>\n" +
                "    <h1 style=\"margin-top: 250px\">Dear " + name + ", your order: </h1><br>\n" +
                "<section>\n" +
                "<p>" + goods.toString() + "</p>\n" +
                "</section>" +
                "<p>Total: $ " + total + "</p><br>\n" +
                "   </div>\n" +
                "</body>\n" +
                "</html>");
    }
}

