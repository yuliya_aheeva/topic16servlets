package by.training;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SecondPageServlet extends HttpServlet {
    private HashMap<Item, Integer> basket = new HashMap<>();
    private ArrayList<Item> list = new ArrayList<>();

    public void init() {
        list.add(new Item("Book", 5.5));
        list.add(new Item("Pen", 0.8));
        list.add(new Item("Newspaper", 1.0));
        list.add(new Item("Ruler", 1.2));
    }

    private static final String CSS_CONTAINER_WITH_CENTERED_ELEMENTS = "\"display: -webkit-flex;\n" +
            "display: flex;\n" +
            "align-items: center;\n" +
            "justify-content: center;\n" +
            "height: 600px;\"";

    private static final String ERROR_PAGE = "<html>" +
            "<body>" +
            "<div style=" + CSS_CONTAINER_WITH_CENTERED_ELEMENTS + ">" +
            "<h1> Sorry, checkbox of terms should be checked. <h1>" +
            "</div>" +
            "</body> " +
            "</html>";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        init();
        HttpSession session = req.getSession();
        Map<String, String[]> params = req.getParameterMap();
        if (params.isEmpty() || !params.containsKey("terms")) {
            resp.getWriter().println(ERROR_PAGE);
        } else {
            String name = params.get("name")[0];
            session.setAttribute("user", name);
            resp.getWriter().println(
                    "<html>\n" +
                            "<head>\n" +
                            "    <meta charset=\"UTF-8\">\n" +
                            "    <title>welcome</title>\n" +
                            "</head>\n" +
                            "<body>\n" +
                            "    <div align=\"center\" margin>\n" +
                            "    <h1 style=\"margin-top: 250px\">Hello " + name + "!</h1><br>\n" +
                            "    <form  method=\"POST\">\n" +
                            "       <p>Make your order:</p>\n" +
                            "       <select name=\"goods\">\n" +
                            "       <option selected value=\"s1\">" + list.get(0).toString() + "</option>\n" +
                            "       <option value=\"s2\">" + list.get(1).toString() + "</option>\n" +
                            "       <option value=\"s3\">" + list.get(2).toString() + "</option>\n" +
                            "       <option value=\"s4\">" + list.get(3).toString() + "</option>\n" +
                            "       </select>\n" +
                            "       <br><br>\n" +
                            "           <input type=\"submit\" name=\"add\" value=\"Add Item\">\n" +
                            "           <input type=\"submit\" name=\"submit\" value=\"Submit\"><br><br>\n" +
                            "   </form>" +
                            "   </div>\n" +
                            "</body>\n" +
                            "</html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String[] price  = req.getParameterValues("goods");
        HttpSession session = req.getSession();

        if (req.getParameter("add") != null) {
            doGet(req, resp);
            for (String s : price) {
                int count = 1;
                if (s.equals("s1")) {
                    if (basket.containsKey(list.get(0))) {
                        count = basket.get(list.get(0)) + 1;
                    }
                    basket.put(list.get(0), count) ;
                }
                if (s.equals("s2")) {
                    if (basket.containsKey(list.get(1))) {
                        count = basket.get(list.get(1)) + 1;
                    }
                    basket.put(list.get(1), count) ;
                }
                if (s.equals("s3")) {
                    if (basket.containsKey(list.get(2))) {
                        count = basket.get(list.get(2)) + 1;
                    }
                    basket.put(list.get(2), count) ;
                }
                if (s.equals("s4")) {
                    if (basket.containsKey(list.get(3))) {
                        count = basket.get(list.get(3)) + 1;
                    }
                    basket.put(list.get(3), count) ;
                }
            }
            session.setAttribute("basket", basket);
            resp.getWriter().println("<h3 align=center>" + basket.toString() + "</h3>");
        }
        if (req.getParameter("submit") != null) {
            resp.sendRedirect(req.getContextPath() + "/sum");
            session.setAttribute("basket", basket);
            double total = 0;
            for (Map.Entry<Item, Integer> entry : basket.entrySet()) {
                total += entry.getKey().getPrice() * entry.getValue();
            }
            session.setAttribute("total", total);
        }
    }
}
