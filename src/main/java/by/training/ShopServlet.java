package by.training;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

import static java.text.MessageFormat.format;

public class ShopServlet extends HttpServlet {

    public static final String SHOP_PAGE = "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>start</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "    <div align=\"center\" margin>\n" +
            "    <h1 style=\"margin-top: 250px\">Welcome to Online Shop</h1><br>\n" +
            "    <form  action=\"/welcome\">\n" +
            "        <label>\n" +
            "            <input type=\"text\" size=\"20\" value=\"\" placeholder=\"Enter your name\" name=\"name\"><br>\n" +
            "        </label><br>\n" +
            "        <input type=\"checkbox\" name=\"terms\" value=\"checked\">" +
            "        <label for=\"terms\"> I agree with the terms of service</label><br><br>\n" +
            "        <input type=\"submit\" value=\"Enter\"><br><br>\n" +
            "    </form>\n" +
            "    </div>\n" +
            "</body>\n" +
            "</html>";

    @Override
    public void service(ServletRequest request, ServletResponse response) throws IOException {
        response.getWriter().println(format(SHOP_PAGE));

    }
}
